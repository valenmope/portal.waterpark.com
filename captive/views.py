from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.utils.translation import get_language
from rest_framework.response import Response
from rest_framework import status
import tools_utils
import constants
import logging
import urllib
import uuid


logger = logging.getLogger(__name__)

def home(request):
	logger.debug('New home request')

	user_mac = request.GET.get('id', '')[:24]
	router_mac = request.GET.get('ap', '')[:24]
	redirect_url = request.GET.get('url', '')[:1024]
	template = 'index.html'
	logger.debug('Captive portal loaded by url rquest: ' + str(redirect_url))
		
	response = render(request, template, {'redirect_url': redirect_url, 'user_mac': user_mac, 'router_mac': router_mac})
	return response

def register(request):
	logger.debug('Register request')

	if request.method == 'POST':
		logger.debug(request.POST)

		email = request.POST.get('email')
		first_name =request.POST.get('first_name')
		last_name = request.POST.get('last_name')
		tools_utils.send_user_data(email, first_name, last_name)

		response = HttpResponseRedirect('/success/?id='+request.POST.get('id', '')+'&ap='+request.POST.get('ap', '')+'&url='+request.POST.get('url', ''))
	else:
		user_mac = request.GET.get('id', '')[:24]
		router_mac = request.GET.get('ap', '')[:24]
		redirect_url = request.GET.get('url', '')[:1024]
		template = 'register.html'
			
		response = render(request, template, {'redirect_url': redirect_url, 'user_mac': user_mac, 'router_mac': router_mac})

	return response

def success(request):
	logger.debug('User successfully logged in. Show success page')

	#tools_utils.grant_access(request.GET.get('id'), request.GET.get('ap'), request.GET.get('url'))
	template = 'success.html'

	response = render(request, template)
	return response

def custom_404(request):
	logger.warn('Error 404 - Request GET data: ' + str(request.GET) + ' - Request POST data: ' + str(request.POST))
	response_data = {}
	return render(request, '404.html', response_data)

def custom_500(request):
	logger.error('Error 500 - Request GET data: ' + str(request.GET) + ' - Request POST data: ' + str(request.POST))
	response_data = {}
	return render(request, '500.html', response_data)