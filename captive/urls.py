from captive import views
from django.conf.urls import patterns, url

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browseable API.
urlpatterns = patterns('',
    url(r'^$', views.home),
    url(r'^home/$', views.home),
    url(r'^guest/s/default/$', views.home), 
    url(r'^register/$', views.register),
    url(r'^success/$', views.success),
)
