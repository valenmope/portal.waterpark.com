from django.conf import settings
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.utils.encoding import smart_str
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from django.conf import settings
import smtplib
import tools_ubiquity
import logging
import time

logger = logging.getLogger(__name__)

def send_user_data(email, first_name, last_name):
	msg = MIMEMultipart('alternative')
	msg['Subject'] = settings.CONTACT_EMAIL_SUBJECT
	msg['From'] = 'noreply@waterworldwaterpark.com'
	msg['To'] = settings.CONTACT_EMAIL

	# Create the body of the message (a plain-text and an HTML version).
	text = "User email: " + smart_str(email) + '\n'
	text += "User name: " + smart_str(first_name) + ' ' + smart_str(last_name) + '\n'
	
	part1 = MIMEText(text, 'plain')
	msg.attach(part1)

	# Send the message via local SMTP server.
	s = smtplib.SMTP('localhost')
	s.sendmail(msg['From'], msg['To'], msg.as_string())
	s.quit()  

	logger.debug('Email sent to: ' + settings.CONTACT_EMAIL)
	logger.debug('Content: ' + text)


def grant_access(user_mac, router_mac, redirect_url=''):
	logger.debug('First grant access against UniFi Controller')
	cookie = tools_ubiquity.login()
	tools_ubiquity.authorize(user_mac, router_mac, settings.SESSION_TIME_MINUTES, cookie)
	tools_ubiquity.logout(cookie)

	logger.debug('Last, wait some seconds unti UniFi Controller connects to the AP')
	time.sleep(settings.UNIFI_ACCESS_DELAY)

	logger.debug('Done. Access granted to user mac: ' + str(user_mac) + ' to router mac: ' + str(router_mac) + '. Redirect url set to ' + str(redirect_url))
