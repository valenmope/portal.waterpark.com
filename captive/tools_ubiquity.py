from django.conf import settings
import pycurl
import urllib
import json
import uuid
import os

#curl --cookie /tmp/unifi_cookie --cookie-jar /tmp/unifi_cookie --insecure https://unifi.beta.cheerz.co:8443/login -d "login=login" -d "username=mgmtcheerzcontroller" -d "password=m(?1(LHbFVEo>-SAMTp"
def login():
	cookie_name = 'ubnt_cookie_' + str(uuid.uuid4())
	cookie_full_path = '/tmp/' + cookie_name

	data = {'login' : 'login', 'username' : settings.UNIFI_USERNAME, 'password' : settings.UNIFI_PASSWORD}
	c = pycurl.Curl()
	c.setopt(pycurl.URL, settings.UNIFI_LOGIN_URL + urllib.urlencode(data))
	c.setopt(pycurl.COOKIEJAR, cookie_full_path)
	c.setopt(pycurl.SSL_VERIFYPEER, 0)   
	c.setopt(pycurl.SSL_VERIFYHOST, 0)
	#c.setopt(pycurl.WRITEDATA, file("resulta.html","wb")) 
	c.perform()
	c.close()

	return cookie_full_path

#curl --cookie /tmp/unifi_cookie --cookie-jar /tmp/unifi_cookie --insecure https://unifi.beta.cheerz.co:8443/api/s/default/cmd/stamgr --data "json={'cmd':'authorize-guest', 'mac':'ac:81:12:73:6d:0f', 'minutes':2, 'ap_mac':'04:18:d6:86:6a:32'}"
def authorize(user_mac, ap_mac, session_time, cookie_full_path):
	data = {'cmd' : 'authorize-guest', 'mac' : user_mac, 'minutes' : session_time, 'ap_mac' : ap_mac}
	data = 'json=' + json.dumps(data)
	c = pycurl.Curl()
	c.setopt(pycurl.URL, settings.UNIFI_ACCESS_URL)
	c.setopt(pycurl.COOKIEFILE, cookie_full_path)
	c.setopt(pycurl.SSL_VERIFYPEER, 0)   
	c.setopt(pycurl.SSL_VERIFYHOST, 0)
	c.setopt(pycurl.POST, 1)
	c.setopt(pycurl.POSTFIELDS, data)
	#c.setopt(pycurl.WRITEDATA, file("resultb.html","wb")) 
	c.perform()
	c.close()

#curl --cookie /tmp/unifi_cookie --cookie-jar /tmp/unifi_cookie --insecure https://unifi.beta.cheerz.co:8443/logout
def logout(cookie_full_path):
	c = pycurl.Curl()
	c.setopt(pycurl.URL, settings.UNIFI_LOGOUT_URL)
	c.setopt(pycurl.COOKIEJAR, cookie_full_path)
	c.setopt(pycurl.SSL_VERIFYPEER, 0)   
	c.setopt(pycurl.SSL_VERIFYHOST, 0)
	#c.setopt(pycurl.WRITEDATA, file("resultc.html","wb")) 
	c.perform()
	c.close()

	os.remove(cookie_full_path)

