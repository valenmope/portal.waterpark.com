$(document).ready(function() {

    // Check for autofills, will be fired when selecting Register or Login
    function comprobarContenido(){
        var $valorC = $('input').val().length;
        if ($valorC > 0) {
            $('input').addClass('used');
        }
    }

    function chequea(){
        setInterval(comprobarContenido, 100);
    }

    // UI elements
    $('#loginemail').click(function() {
        $('#logindiv').toggleClass('hidden').show();
        $('#loginbuttons, #brand, #welcome, #terms').hide();
        $('body').css('background-color', '#F5F5F5');
    });
    $('#loginemail').on('click', chequea);
    $('#registeremail').on('click', chequea);
    $('#closelogin').click(function() {
        $('#logindiv').toggleClass('hidden').hide();
        $('#loginbuttons, #brand, #welcome, #terms').show();
        $('body').css('background-color', '#d0555b');
    });
    $('#registeremail').click(function() {
        $('#registerdiv').toggleClass('hidden').show();
        $('#content').toggleClass('content');
        $('#loginbuttons, #brand, #welcome, #terms').hide();
        $('body').css('background-color', '#F5F5F5');
    });
    $('#closeregister').click(function() {
        $('#registerdiv').toggleClass('hidden').hide();
        $('#content').toggleClass('content');
        $('#loginbuttons, #brand, #welcome, #terms').show();
        $('body').css('background-color', '#d0555b');
    });
    $('#addphoto').click(function() {
        $('#photochoosediv, #photoplaceholder').toggleClass('hidden');
    });
});

// Input Styling
$(window, document, undefined).ready(function() {
    $('input').blur(function() {
        var $this = $(this);
        if ($this.val()) $this.addClass('used');
        else $this.removeClass('used');
    });
    var $ripples = $('.ripples');
    $ripples.on('click.Ripples', function(e) {
        var $this = $(this);
        var $offset = $this.parent().offset();
        var $circle = $this.find('.ripplesCircle');
        var x = e.pageX - $offset.left;
        var y = e.pageY - $offset.top;
        $circle.css({
            top: y + 'px',
            left: x + 'px'
        });
        $this.addClass('is-active');
    });
    $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
        $(this).removeClass('is-active');
    });
}); + function($) {
    'use strict';}