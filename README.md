waterworld-captive-portal
=============================

Captive portal application for WaterWorld 

Installation
------------

```
    $ python setup.py install
    $ pip uninstall djangorestframework
    $ pip install djangorestframework
    $ python manage.py clearsessions
    $ python manage.py syncdb
    $ uwsgi --module=configuration.wsgi:application --env DJANGO_SETTINGS_MODULE=configuration.settings --http-socket :8063 -b 32768 --harakiri=20 --max-requests=5000 --vacuum

```

Requirements
------------

* Python_ 2.7+ (not tested with lower version)
* Django_ 1.7+ (may work on lower version, but not tested)

.. _Python: https://python.org
.. _Django: http://djangoproject.com
.. _django-tastypie: http://tastypieapi.org
.. _ElasticSearch: http://elasticsearch.org

How to use?
-----------

* See documentation: http://waterworldwaterpark.com/
* Mailing list: http://waterworldwaterpark.com/
