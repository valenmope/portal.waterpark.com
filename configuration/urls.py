from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from captive import views

admin.autodiscover()

urlpatterns = patterns('',
   url(r'^',include('captive.urls')),
)

urlpatterns += staticfiles_urlpatterns()

handler404 = views.custom_404
handler500 = views.custom_500
