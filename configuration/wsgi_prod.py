SITE_DIR = '/home/azureuser/development/portal.waterpark.com'
import site
site.addsitedir(SITE_DIR) 

import os
import sys
sys.path.append(SITE_DIR)

os.environ['DJANGO_SETTINGS_MODULE'] = 'configuration.settings_prod'
import django.core.wsgi
application =django.core.wsgi.get_wsgi_application()
