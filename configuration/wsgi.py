SITE_DIR = '/home/valentin/freelance/portal.waterworldwaterpark.com'
import site
site.addsitedir(SITE_DIR) 

import os
import sys
sys.path.append(SITE_DIR)

os.environ['DJANGO_SETTINGS_MODULE'] = 'configuration.settings'
import django.core.wsgi
application =django.core.wsgi.get_wsgi_application()
